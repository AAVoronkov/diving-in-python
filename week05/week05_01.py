import socket
import time

class ClientError(Exception):
    pass

class Client():
    
    def __init__(self, address, port, timeout=None):
        self.address = address
        self.port = int(port)
        self.timeout = int(timeout)
          
    def put(self, metric_name, metric_value, timestamp=None):
        try:
            self.sock = socket.create_connection((self.address, self.port), self.timeout)
            self.sock.sendall("put {} {} {}\n".format(metric_name, metric_value, timestamp or int(time.time())).encode())
            response = self.sock.recv(1024).decode()
            if response[0:2] != 'ok':
                raise ClientError(response)
            return response
            self.sock.close()
        except socket.error as err:
            raise ClientError(err)

    def get(self, metric_name):
        try:
            self.sock = socket.create_connection((self.address, self.port), self.timeout)
            self.sock.sendall("get {}\n".format(metric_name).encode())
            response = self.sock.recv(1024).decode()
            if response[0:2] != 'ok':
                raise ClientError(response)
            metric_data = {}
            metric_list = response[3:-2].split('\n')
            if metric_list == ['']:
                return metric_data
            else:
                for metric in metric_list:
                    units = metric.split(' ')
                    metric_name = units[0]
                    metric_value = float(units[1])
                    timestamp = int(units[2])
                    if not metric_name in metric_data:
                        metric_data[metric_name] = []
                    metric_data[metric_name].append((timestamp, metric_value))
                return metric_data
            self.sock.close()
        except socket.error as err:
            raise ClientError(err)

client = Client("127.0.0.1", 8888, timeout=15)

client.put("palm.cpu", 0.5, timestamp=1150864247)
client.put("palm.cpu", 2.0, timestamp=1150864248)
client.put("palm.cpu", 0.5, timestamp=1150864248)

client.put("eardrum.cpu", 3, timestamp=1150864250)
client.put("eardrum.cpu", 4, timestamp=1150864251)
client.put("eardrum.memory", 4200000)