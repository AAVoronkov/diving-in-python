import asyncio

metric_data = {}

class ClientServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        response = data.decode()
        command = response.split(' ')[0]
        if command == "put":
            metric_name = response.split(' ')[1]
            metric_value = response.split(' ')[2]
            timestamp = response.split(' ')[3][:-1]
            if not metric_name in metric_data:
                metric_data[metric_name] = []
            if not (timestamp, metric_value) in metric_data[metric_name]:
                metric_data[metric_name].append((timestamp, metric_value))
            self.transport.write('ok\n\n'.encode())
        elif command == "get":
            metric_name = response.split(' ')[1][:-1]
            string = 'ok\n'
            if metric_name == '*':
                for metric, metric_values in metric_data.items():
                    for metric_value in metric_values:
                        string = string + metric + ' ' + str(metric_value[1]) + ' ' + str(metric_value[0]) + '\n'
                self.transport.write((string + '\n').encode())
            elif metric_name in metric_data:
                for metric_value in metric_data[metric_name]:
                    string = string + metric_name + ' ' + str(metric_value[1]) + ' ' + str(metric_value[0]) + '\n'
                self.transport.write((string + '\n').encode())
            else:
                self.transport.write('ok\n\n'.encode())
        else:
            self.transport.write('error\nwrong command\n\n'.encode())

def run_server(address, port):
    loop = asyncio.get_event_loop()
    coro = loop.create_server(ClientServerProtocol, address, port)
    server = loop.run_until_complete(coro)
    
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()

run_server('127.0.0.1', 8888)