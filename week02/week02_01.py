import os
import tempfile
import sys
import argparse
import json

key_value_data = {}
value_list = list()
storage_path = os.path.join(tempfile.gettempdir(), 'storage.data')
if not os.path.exists(storage_path):
   with open(storage_path, 'w') as f:
        json.dump({}, f)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument ('--key', nargs='?', action='store', default='', dest='key')
    parser.add_argument ('--val', nargs='+', action='store', dest='val')
    namespace = parser.parse_args(sys.argv[1:])

    if not namespace.key:
        print(None)
            
    else:
        if namespace.val != None:
            
            key_name = namespace.key
            value = namespace.val

            with open(storage_path, 'r') as f:
                key_value_data = json.load(f)
            
                if key_name in key_value_data:
                    old_value = key_value_data[key_name]
                    value_list.extend(old_value)
                    value_list.extend(value)
                    key_value_data[key_name] = value_list
                    with open(storage_path, 'w') as f:
                        json.dump(key_value_data, f)

                else:
                    key_value_data[key_name] = value
                    with open(storage_path, 'w') as f:
                        json.dump(key_value_data, f)

        else:
            key_name = namespace.key
            with open(storage_path, 'r') as f:
                key_value_data = json.load(f)
                if (key_name in key_value_data):
                    if type(key_value_data[key_name]) == list:
                        print(', '.join(key_value_data[key_name]))
                    else:
                        print(key_value_data[key_name])
                else:
                    print(None)
