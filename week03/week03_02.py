import os
import csv


class CarBase:
    
    def __init__(self, brand, photo_file_name, carrying):
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = carrying

    def get_photo_file_ext(self):
        return os.path.splitext(self.photo_file_name)[1]
    
    
class Car(CarBase):
    
    def __init__(self, car_type, brand, photo_file_name, carrying, passenger_seats_count):
        self.car_type = car_type
        super().__init__(brand, photo_file_name, carrying)
        self.passenger_seats_count = int(passenger_seats_count)


class Truck(CarBase):
    
    def __init__(self, car_type, brand, photo_file_name, carrying, body_whl):
        
        self.car_type = car_type
        super().__init__(brand, photo_file_name, carrying)
        self.body_whl = body_whl
        try:
            length, width, height = (float(i) for i in body_whl.split('x', 2))
        except ValueError:
             length, width, height = 0.0, 0.0, 0.0
        self.body_length = length
        self.body_width = width
        self.body_height = height
            
    def get_body_volume(self):
        return self.body_length * self.body_width * self.body_height 


class SpecMachine(CarBase):
    
    def __init__(self, car_type, brand, photo_file_name, carrying, extra):
        self.car_type = car_type
        super().__init__(brand, photo_file_name, carrying)
        self.extra = extra


def get_car_list(file_path):
    
    car_list = []
    
    if not os.path.isfile(file_path):
        return car_list
        
    with open(file_path) as csv_fd:
        reader = csv.reader(csv_fd, delimiter=';')
        next(reader)  # пропускаем заголовок
    
        for row in reader:
            
            if len(row) == 7:
            
                if 'car' in row:
                    car_list.append(Car(car_type = row[0], brand = row[1], passenger_seats_count = row[2], photo_file_name = row[3], carrying = row[5]))
                    
                elif 'truck' in row:
                    car_list.append(Truck(car_type = row[0], brand = row[1], photo_file_name = row[3], body_whl = row[4], carrying = row[5]))
                
                elif 'spec_machine' in row:
                    car_list.append(SpecMachine(car_type = row[0], brand = row[1], photo_file_name = row[3], carrying = row[5], extra = row[6]))
            
                else:
                    pass
                
            else:
                pass

    return car_list
    
get_car_list('coursera_week3_cars.csv')
