import os
import sys

class FileReader:
    
    def __init__(self, path=None):
        self.path = path

    def read(self):
        try:
            with open(self.path, 'r') as f:
                return f.read()
        except IOError:
            return ""

if __name__ == '__main__':
    path = sys.argv[1]
    reader = FileReader(path)
    print(reader.read())
