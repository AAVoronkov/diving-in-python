import os
import tempfile

class File:
    
    def __init__(self, path):
        self.path = path
        if not os.path.exists(self.path):
            open(self.path, 'w').close()
        self.point = 0
    
    def read_data(self):
        with open(self.path, 'r') as f:
            return f.read()
    
    def write(self, data):
        with open(self.path, 'w') as f:
            return f.write(data)
    
    def __add__(self, other):
        first_data = self.read_data()
        second_data = other.read_data()
        result_data = first_data + second_data
        result_file = File(os.path.join(tempfile.gettempdir(), 'result_file.txt'))
        result_file.write(result_data)
        return result_file
    
    def __iter__(self):
        return self
        
    def __next__(self):
        with open(self.path, 'r') as f:
            f.seek(self.point)
            line = f.readline()
            if not line:
                self.point = 0
                raise StopIteration
            self.point = f.tell()
            return line
    
    def __str__(self):
        return os.path.join(tempfile.gettempdir(), self.path)